import pytest
import json
import logging
from threading import Event
logger = logging.getLogger(__name__)


@pytest.fixture(autouse=True)
def data():
    return {
        'id': 12345,
        'title': 'This is title',
        'name': 'Jaewan Kim'
    }


@pytest.fixture(autouse=True)
def simple_note():
    from ..common_builders import value_by_key as simple_build, async_delay_build, sync_delay_build
    from ..payload_builder import build
    from rx import Observable as O
    return [
        ('id', lambda d: async_delay_build(d, 'id', 1.0)),
        ('title', lambda d: async_delay_build(d, 'title', 1.0)),
        ('name', lambda d: async_delay_build(d, 'name')),
        ('metadata', lambda d: build(d, simple_metadata()).zip(O.of('metadata'), lambda v, k: (k, v)))
    ]


@pytest.fixture(autouse=True)
def simple_metadata():
    from ..common_builders import value_by_key as simple_build, async_delay_build, sync_delay_build
    return [
        ('id', lambda d: async_delay_build(d, 'id', 1.0)),
        ('title', lambda d: async_delay_build(d, 'title', 1.0)),
        ('name', lambda d: async_delay_build(d, 'name', 1.0))
    ]


@pytest.fixture(autouse=True)
def data_with_list():
    return {
        'id': 12345,
        'title': 'This is title',
        'name': 'Jaewan',
        'bits': [
            'Bit 1',
            'Bit 2'
        ]
    }


@pytest.fixture(autouse=True)
def note_with_list():
    from ..common_builders import value_by_key as simple_build, async_delay_build, sync_delay_build
    from ..payload_builder import build_list, build
    from rx import Observable as O
    return [
        ('id', lambda d: sync_delay_build(d, 'id', 1.0)),
        ('title', lambda d: sync_delay_build(d, 'title', 1.0)),
        ('metadata', lambda d: build(d, simple_metadata()).map(lambda m: ('metadata', m))),
        ('bits', lambda d: build_list(d, 'bits').delay(0))
    ]


def test_build():
    from rx import Observable as O
    from ..payload_builder import build
    from time import sleep

    def verify(value):
        logger.info(json.dumps(value, indent=2))
        assert value

    def verify_exp(exp):
        logger.info(exp)
        assert exp
        return O.empty()

    def run(time_limit):
        hold = Event()
        O.of(123)\
            .do_action(lambda n: hold.set())\
            .do_action(lambda n: logger.info('Starting... {}'.format(time_limit)))\
            .map(lambda n: data_with_list())\
            .flat_map(lambda d: build(d, note_with_list()))\
            .take_until(O.timer(time_limit))\
            .first()\
            .do_action(verify)\
            .catch_exception(verify_exp)\
            .finally_action(lambda: hold.clear())\
            .subscribe()

        while hold.is_set():
            sleep(0.001)

    run(3000)
    run(4000)
    run(5000)
    run(6000)
    run(7000)
    run(8000)


def test_build_list():
    from rx import Observable as O
    from ..payload_builder import build_list
    from ..common_builders import build_bit
    from time import sleep

    def verify(value):
        assert isinstance(value, tuple)
        assert len(value) == 2
        logger.info(value)

    hold = Event()
    O.of(123) \
        .do_action(lambda n: hold.set()) \
        .do_action(lambda n: logger.info('Starting... {}'.format(2000))) \
        .map(lambda n: data_with_list()) \
        .flat_map(lambda d: build_list(d, 'bits', build_bit)) \
        .take_until(O.timer(2000)) \
        .first()\
        .do_action(verify)\
        .finally_action(lambda: hold.clear()) \
        .subscribe()

    while hold.is_set():
        sleep(0.001)
