from rx import Observable as O
from .common_builders import value_by_key as simple_build, sync_delay_build, async_delay_build
from .payload_builder import build


def note():
    return [
        ('id', lambda d: simple_build(d, 'id')),
        ('title', lambda d: sync_delay_build(d, 'title')),
        ('name', lambda d: async_delay_build(d, 'name')),
        ('metadata', lambda d: build(d, metadata()).map(lambda m: ('metadata', m)))
    ]


def metadata():
    return [
        ('id', lambda d: sync_delay_build(d, 'id')),
        ('title', lambda d: sync_delay_build(d, 'title')),
        ('name', lambda d: async_delay_build(d, 'name'))
    ]
