from rx import Observable as O
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)


def simple_build(data, key):
    return O.of(data)\
        .pluck(key)\
        .zip(O.of(key), lambda v, k: (k, v))


def delay_build(data, key, delay=500):
    return simple_build(data, key).delay(delay)


def builders(data):
    return [
        delay_build(data, 'id'),
        delay_build(data, 'title'),
        delay_build(data, 'name')
    ]


def combine(out, builder):
    out[builder[0]] = builder[1]
    return out


def build(data):

    placeholder = OrderedDict([
        ('id', None),
        ('title', None),
        ('name', None),
    ])

    return O.of(data)\
        .do_action(logger.debug)\
        .flat_map(lambda d: O.merge(builders(d)))\
        .do_action(logger.debug)\
        .reduce(combine, placeholder)