import pytest


@pytest.fixture
def bits_data():
    return {
        'bits': [
            {
                'id': 1234,
                'title': 'This is the title'
            },
            {
                'id': 5467,
                'title': 'This is another title'
            }
        ]
    }


@pytest.mark.skip
def test_build_bits():
    from ..common_builders import build_list
    build_list(bits_data()).subscribe(print)
