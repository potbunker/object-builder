from rx import Observable as O
import logging
logger = logging.getLogger(__name__)


def value_by_key(data, key):
    return O.of(data).pluck(key).map(lambda v: (key, v))


def async_delay_build(data, key, delay=0.5):
    return value_by_key(data, key).delay(delay * 1000)


def sync_delay_build(data, key, delay=0.5):
    from time import sleep
    sleep(delay)
    return value_by_key(data, key)


def build_bit(data, bit):
    return O.of(bit).delay(1000)


