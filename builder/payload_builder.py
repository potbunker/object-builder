from rx import Observable as O
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)


def inspect(value):
    logger.debug(value)


def combine(out, result):
    out[result[0]] = result[1]
    return out


def build(data, plan):

    def validate(p):
        assert p and isinstance(p, list)
        for name, func in p:
            assert name and isinstance(name, str)
            assert func and callable(func) and issubclass(type(func(data)), O)

    validate(plan)

    keys, funcs = zip(*plan)
    template = map(lambda k: (k, None), keys)

    return O.of(data)\
        .do_action(inspect)\
        .flat_map(lambda d: O.from_(funcs).map(lambda f: f(d)).merge_all())\
        .do_action(inspect)\
        .reduce(combine, OrderedDict(template))


def build_list(data, key, builder=lambda d, e: O.of(e)):
    template = [None] * len(data[key])

    return O.of(data).pluck(key) \
        .map(enumerate) \
        .flat_map(lambda es: O.from_(es)
                  .map(lambda e: builder(data, e[1]).map(lambda v: (e[0], v)))
                  .merge_all()) \
        .do_action(inspect)\
        .reduce(combine, template)\
        .map(lambda l: (key, l))

